// Nomenclatura de variáveis

const titleUserGithub = [
    {
      title: 'User',
      followers: 5
    },
    {
      title: 'Friendly',
      followers: 50,
    },
    {
      title: 'Famous',
      followers: 500,
    },
    {
      title: 'Super Star',
      followers: 1000,
    },
  ]
  
  export default async function getData(req, res) {
    const userGithub = String(req.query.username)
  
    if (!userGithub) {
      return res.status(400).json({
        message: `Please provide an username to search on the github API`
      })
    }
  
    const searchUserGithub = await fetch(`https://api.github.com/users/${userGithub}`);
  
    if (searchUserGithub.status === 404) {
      return res.status(400).json({
        message: `User with username "${userGithub}" not found`
      })
    }
  
    const dataUserGithub = await response.json()
  
    const orderByUserGihub = titleUserGithub.sort((a, b) =>  b.followers - a.followers); 
  
    const categoryUserGithub = orderByUserGihub.find(i => dataUserGithub.followers > i.followers)
  
    const result = {
        userGithub,
        categoryUserGithub: categoryUserGithub.title
    }
  
    return result
  }
  
  getData({ query: {
    username: 'josepholiveira'
  }}, {})